package com.music.list.fileupload;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/file")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class ProductController {
    ProductService productService;

    @PostMapping("/add")
    public void addFile(@RequestParam MultipartFile file,
            @RequestBody ProductDto productDto) throws IOException {

        String fileName = file.getOriginalFilename();

        assert fileName != null;
        file.transferTo( new File( fileName));
    productService.addFile(file,productDto);
    }

    @PostMapping("/upload")
    public void handleFileUpload(@RequestParam("file") MultipartFile file ) throws IOException {

       productService.add(file);

    }

    @GetMapping("/get")
    public List<Product> get( ) {

      return   productService.get();

    }
}
