package com.music.list.fileupload;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class ProductService {
    ProductRepo productRepo;
    ProductMapper productMapper;

    public void addFile(MultipartFile file,
                        ProductDto productDto) throws IOException {


       // Product pro=productMapper.toProduct(productDto);
//        pro.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
       // productRepo.save(pro);
    }

    public void add(MultipartFile file) throws IOException {


        String fileName = file.getOriginalFilename();

        assert fileName != null;
        file.transferTo( new File( fileName));

    Product product=new Product();
    product.setImage(file.getName());

        productRepo.save(product)   ;
    }

    public List<Product> get(){
        return  productRepo.findAll();
    }
}
