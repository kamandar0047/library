package com.music.list.fileupload;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)

public class ProductDto {
    String name;
    String description;
    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    private String image;
}

