package com.music.list.service;

import com.music.list.dto.SingerDto;
import com.music.list.dto.SongDto;
import com.music.list.page.SingFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SingerService {
    void create(SingerDto singerDto);

    SingerDto edit(Long id,SingerDto singerDto);

    void delete(Long id);

    Page<SongDto> findAll();

    Page<SingerDto> findAllSinger(SingFilter filter, Pageable pageable);

}
