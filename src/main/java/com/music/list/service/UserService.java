package com.music.list.service;

import com.music.list.constant.Role;
import com.music.list.security.User;
import com.music.list.security.UserDto;

import java.util.List;

public interface UserService {
    void create(UserDto userDto);

    void changeRole(Long id, Long roleId);

    void add();

    List<User> findUser();
}
