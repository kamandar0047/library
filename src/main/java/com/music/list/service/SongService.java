package com.music.list.service;

import com.music.list.dto.SongDto;
import com.music.list.page.SongFilter;
import com.music.list.response.SongResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SongService {
    void create(SongDto songDto);

    SongDto edit(Long id,SongDto songDto);

    void delete(Long id);

    Page<SongDto> findAll(SongFilter songFilter, Pageable pageable);
    Page<SongDto> findSingerId(Long id,SongFilter songFilter, Pageable pageable);

    void addSong(Long id,Long singerId);

    void scheduled();

    SongResponse music(Long id);


}
