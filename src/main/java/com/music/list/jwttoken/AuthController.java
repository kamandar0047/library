package com.music.list.jwttoken;

import com.music.list.security.UserDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthController {
    JwtService jwtService;

    @PostMapping("/sign-in")
    public JWTDto signIn(@RequestBody UserDto authentication){

        return new JWTDto(jwtService.issueToken(authentication));
    }
}
