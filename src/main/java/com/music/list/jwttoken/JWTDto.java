package com.music.list.jwttoken;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JWTDto {

    private String accessToken;
}
