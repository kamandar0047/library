package com.music.list.redis;

import com.music.list.domain.Singer;
import com.music.list.dto.SingerDto;
import com.music.list.mapper.SingerMapper;
import com.music.list.page.SingFilter;
import com.music.list.repository.SingerRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;


import javax.persistence.criteria.Predicate;
import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class RedisService {
    SingerRepository singerRepository;
    SingerMapper singerMapper;


    @Cacheable(value = "findAll",key="#lang")
    //@CachePut()-redisde edit metodu
    //@CacheEvict()-redisde delete metodu
    public List<SingerDto> findAll(String lang) {

        return singerMapper.singerDtoList(singerRepository.findAll());
    }
}
