package com.music.list.redis;

import com.music.list.dto.SingerDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/redis/singer")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class RedisController {
    RedisService redisService;
    private static final String ACCEPT_LANGUAGE = "Accept-Language";
    private static final String DEF_LANGUAGE = "az";



    @GetMapping("/find-all-redis")
    public List<SingerDto> findAll(HttpServletRequest servletRequest){
        String language =
                servletRequest.getHeader(ACCEPT_LANGUAGE) != null ? servletRequest.getHeader(ACCEPT_LANGUAGE) :
                        DEF_LANGUAGE;

        return redisService.findAll(language);
    }
}
