package com.music.list.repository;

import com.music.list.security.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepo extends JpaRepository<Authority,Long> {

}
