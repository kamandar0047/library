package com.music.list.repository;

import com.music.list.domain.Song;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SongRepository extends JpaRepository<Song,Long> , JpaSpecificationExecutor<Song> {

    Page<Song> findBySinger_Id(Long id,  Pageable pageable);
    List<Song> findBySinger_Id(Long id);
}
