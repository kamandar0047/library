package com.music.list.controller;

import com.music.list.dto.SongDto;
import com.music.list.page.SongFilter;
import com.music.list.response.SongResponse;

import com.music.list.service.impl.SongServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/song")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class SongController {

    SongServiceImpl songService;
   // TaskDefinitionBean taskDefinitionBean;


    @GetMapping("/find-all")
    public Page<SongDto> findAll(SongFilter songFilter) {
       return songService.findAll( songFilter, PageRequest.of(songFilter.getPageNumber(),
               songFilter.getPageSize(),
               Sort.by(Sort.Direction.fromString(songFilter.getDir()),songFilter.getField())));
    }


    @GetMapping("/find-all/id/{id}")
    public Page<SongDto> findId(@PathVariable Long id,SongFilter songFilter) {
        return songService.findSingerId(id, songFilter, PageRequest.of(songFilter.getPageNumber(),
                songFilter.getPageSize(),
                Sort.by(Sort.Direction.fromString(songFilter.getDir()),songFilter.getField())));
    }


    @PutMapping("/add")
    public void addSong(@RequestParam("id") Long id,
                        @RequestParam("singerid") Long singerId) {
        songService.addSong(id,singerId);
    }

    @GetMapping("/music/id/{id}")
    public SongResponse music(@PathVariable Long id) {
        return songService.music(id);
    }



}

