package com.music.list.controller.admincontroller;

import com.music.list.dto.SingerDto;
import com.music.list.dto.SongDto;
import com.music.list.service.impl.SingerServiceImpl;
import com.music.list.service.impl.SongServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/song")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class AdminSongController {

 private final SongServiceImpl songService;

    @PostMapping("/save")

    public void create(@RequestBody SongDto songDto) {
        songService.create(songDto);

    }

    @PutMapping("/edit/id/{id}")
    public SongDto edit(@PathVariable Long id,
                          @RequestBody SongDto songDto) {

        return songService.edit(id,songDto);
    }

    @DeleteMapping("/delete/id/{id}")
    public void delete(@PathVariable Long id) {
        songService.delete(id);
    }
}
