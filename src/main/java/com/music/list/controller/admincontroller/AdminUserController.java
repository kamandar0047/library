package com.music.list.controller.admincontroller;

import com.music.list.security.User;
import com.music.list.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/user")
public class AdminUserController {
    private final UserServiceImpl userService;

    @PutMapping("/edit")
    public void changeRole(@RequestParam("id") Long id,
                           @RequestParam("roleid") Long roleId){
        userService.changeRole(id,roleId);
    }

    @GetMapping("/list")
    public List<User> showList(){
        return userService.findUser();
    }
}
