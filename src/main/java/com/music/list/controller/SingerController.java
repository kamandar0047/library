package com.music.list.controller;

import com.music.list.dto.SingerDto;
import com.music.list.page.SingFilter;
import com.music.list.service.impl.SingerServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/singer")
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class SingerController {
  private final   SingerServiceImpl singerService;

  @GetMapping("/find-all")
  Page<SingerDto> getAll(SingFilter filter) {
    return singerService.findAllSinger(filter, PageRequest.of(filter.getPageNumber(),filter.getPageSize()
            , Sort.by(Sort.Direction.fromString(filter.getDir()), filter.getField())));
  }
}
