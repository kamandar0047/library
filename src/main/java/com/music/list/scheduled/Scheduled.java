package com.music.list.scheduled;


import com.music.list.service.SongService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

@Service
@RequiredArgsConstructor
public class Scheduled implements   Runnable {


  private final TaskScheduler taskScheduler;


 // private final SongService songService;
    Map<String, ScheduledFuture<?>> jobsMap = new HashMap<>();

    public void scheduleATask(String jobId, Runnable tasklet, String cronExpression) {

        String cron="0 "+cronExpression.substring(3,5)+" "+cronExpression.substring(0,2)+" * * *";
        System.out.println(cron);
        ScheduledFuture<?> scheduledTask = taskScheduler.schedule(tasklet, new CronTrigger(cron, TimeZone.getTimeZone(TimeZone.getDefault().getID())));
        jobsMap.put(jobId, scheduledTask);

    }


    @Override
    public void run() {

        //methodu burda yazmaq
     //   System.out.println(songService.);

    }
}
