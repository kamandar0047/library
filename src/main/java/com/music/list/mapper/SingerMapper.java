package com.music.list.mapper;

import com.music.list.domain.Singer;
import com.music.list.domain.Song;
import com.music.list.dto.SingerDto;
import com.music.list.dto.SongDto;
import lombok.experimental.FieldDefaults;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface SingerMapper {
    SingerDto singerDto(Singer sing);
    Singer singer(SingerDto singerDto);


    List<SingerDto> singerDtoList(List<Singer> singerList);
    List<Singer> singerList(List<SingerDto> singerDtoList);
}
