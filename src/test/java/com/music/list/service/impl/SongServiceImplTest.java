package com.music.list.service.impl;

import com.music.list.domain.Singer;
import com.music.list.domain.Song;
import com.music.list.dto.SingerDto;
import com.music.list.dto.SongDto;
import com.music.list.mapper.SongMapper;
import com.music.list.repository.SingerRepository;
import com.music.list.repository.SongRepository;
import com.music.list.response.SongResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SongServiceImplTest {

    @Mock
    SingerRepository singerRepository;
    @Mock
    SongRepository songRepository;
    @Mock
    SongMapper songMapper;
    @InjectMocks
    SongServiceImpl songService;

    Song song;
    SongDto songDto;

    Singer singer;
    @BeforeEach
    void setUp() {
        song=Song.builder()
                .id(1L)
                .name("Lepeler")
                .build();

        songDto=SongDto.builder()
                .name("Lepeler")
                .build();

        singer=Singer.builder()
                .id(1L)
                .name("Shovkat")
                .build();

    }

    @Test
    void create() {
        when(songMapper.song(songDto)).thenReturn(song);
        when(songRepository.save(song)).thenReturn(song);

        songService.create(songDto);

        assertThat(songDto.getName()).isEqualTo("Lepeler");
    }

    @Test
    void edit() {
        when(songRepository.findById(anyLong())).thenReturn(Optional.of(song));
        when(songMapper.songDto(song)).thenReturn(songDto);

        SongDto singerDto1=songService.edit(1L,songDto);

        assertThat(singerDto1.getName()).isEqualTo("Lepeler");


    }

    @Test
    void delete() {
        when(songRepository.findById(anyLong())).thenReturn(Optional.of(song));
        doNothing().when(songRepository).delete(song);

        songService.delete(1L);
    }



    @Test
    void addSong() {
        when(songRepository.findById(anyLong())).thenReturn(Optional.of(song));
        when(singerRepository.findById(anyLong())).thenReturn(Optional.of(singer));

        songService.addSong(1L,1L);

        assertThat(song.getSinger()).isEqualTo(singer);
    }


}